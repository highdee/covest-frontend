import Vue from 'vue'
import App from './App.vue' 
import Vuex from 'vuex';


import Routes from './routes.js'
import DashboardLayout from './components/dashboard/layouts/layout.vue'
import Dashboardnavbar from './components/dashboard/layouts/navbar'
import PageFooter from './components/public_layout/foot'
import PageNav from './components/public_layout/navbar.vue'
import store from './store/index';
import { Datetime } from 'vue-datetime'

import VueCurrencyInput from 'vue-currency-input'

import notification from './components/public_layout/notification';
import loader from './components/public_layout/loader';
import 'vue-datetime/dist/vue-datetime.css'
import MoneyFormat from 'vue-money-format'


Vue.component('user-navbar', Dashboardnavbar)
Vue.component('user-layout', DashboardLayout)
Vue.component('page-nav', PageNav)
Vue.component('page-footer', PageFooter)
Vue.component('notification',notification);
Vue.component('loader',loader);
Vue.component('money-format',MoneyFormat);

Vue.use(Datetime);
Vue.use(Vuex);
Vue.component('datetime', Datetime);
 
const pluginOptions = {
  /* see config reference */
  globalOptions: { currency: 'NGN'}
}
Vue.use(VueCurrencyInput,pluginOptions);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router:Routes,
  store
}).$mount('#app');