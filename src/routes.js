import home from './components/public_pages/home'
import about from './components/public_pages/about'
import faq from './components/public_pages/faq'
import contact from './components/public_pages/contact'
import packages from './components/public_pages/packages'
import login from './components/public_pages/login'
import register from './components/public_pages/register'
import verify from './components/public_pages/verify-account';
import forget from './components/public_pages/forget';
import reset from './components/public_pages/reset';
import userLayout from './components/dashboard/layouts/layout';

import dashboard from './components/dashboard/dashboard.vue'
import fixed from './components/dashboard/fixed-savings';
import createFixed from './components/dashboard/create-fixed-saving';
import editFixed from './components/dashboard/edit-fixed-saving';
import viewFixed from './components/dashboard/view-fixed-saving';
import stash from './components/dashboard/stash-savings';
import investment from './components/dashboard/investment';
import withdraw from './components/dashboard/withdraw';
import card from './components/dashboard/card-details';
import settings from './components/dashboard/settings';

import Router from 'vue-router';
import Vue from 'vue';

Vue.use(Router);

const router = new Router({
    mode:'history',
    routes:[ 
        {
            path:'/dashboard', 
            component: userLayout, 
            children:[
                {
                    path:'', 
                    component: dashboard,
                    meta:{
                        AuthRequired:true
                    },
                },
                {
                    path:'fixed-savings', 
                    component: fixed, 
                    meta:{
                        AuthRequired:true
                    },
                },
                {
                    path:'fixed-savings/create', 
                    component: createFixed, 
                    meta:{
                        AuthRequired:true
                    },
                },
                {
                    path:'fixed-savings/view', 
                    component: fixed, 
                    redirect:'fixed-savings',
                    meta:{
                        AuthRequired:true
                    },
                },
                {
                    path:'fixed-savings/view/:id', 
                    component: viewFixed, 
                    meta:{
                        AuthRequired:true
                    },
                },
                {
                    path:'fixed-savings/edit/:id', 
                    component: editFixed, 
                    meta:{
                        AuthRequired:true
                    },
                },
                {
                    path:'stash-savings', 
                    component: stash, 
                    meta:{
                        AuthRequired:true
                    },
                },
                {
                    path:'investment', 
                    component: investment, 
                    meta:{
                        AuthRequired:true
                    },
                },
                {
                    path:'withdraw', 
                    component: withdraw, 
                    meta:{
                        AuthRequired:true
                    },
                },
                {
                    path:'card-details', 
                    component: card, 
                    meta:{
                        AuthRequired:true
                    },
                },
                {
                    path:'settings', 
                    component: settings, 
                    meta:{
                        AuthRequired:true
                    },
                },
            ],
            meta:{
                AuthRequired:true
            },
        }, 
        {
            path:'/', 
            component: home,
            name: 'home'
        },
        {
            path:'/about-us', 
            component: about,
            name: 'about'
        },
        {
            path:'/faq', 
            component: faq,
            name: 'faq'
        },
        {
            path:'/contact', 
            component: contact,
            name: 'contact'
        },
        {
            path:'/packages', 
            component: packages,
            name: 'packages'
        },
        {
            path:'/login', 
            component: login,
            name: 'login'
        },
        {
            path:'/register', 
            component: register,
            name: 'register'
        },
        {
            path:'/verify-account', 
            component: verify,
            name: 'verifyaccount'
        },
        {
            path:'/forgot-password', 
            component: forget,
            name: 'forget'
        },
        {
            path:'/reset-password/:token', 
            component: reset,
            name: 'reset'
        },
    ]
});

export default router;

router.beforeEach((to,from,next)=>{
    if(to.matched.some(record => record.meta.AuthRequired)) { 
        if (localStorage.getItem('covest') == null) { 
            next({
                path: '/login',
                name:'login' 
            }) 
        } 
        else{ 
            next(); 
        }
    }
    
    next(); 
})