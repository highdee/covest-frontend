export default {
    setNotification(state,data){
        state.notification.type=data.type;
        state.notification.message=data.msg;

       if(!data.unset){
        setTimeout(()=>{
            state.notification.type=0;
        state.notification.message='';
        },3000);
       }
    },
    setUser(state,data){
        state.user=data;
        state.token=data.token;

        var result=encodeURIComponent(JSON.stringify(data));
        localStorage.setItem('covest',result);
    },
    getUser(state){
        var data=localStorage.getItem('covest');    
        data=decodeURIComponent(data); 
        data=JSON.parse(data);

        state.user=data;
        state.token=data.token;

        var result=encodeURIComponent(JSON.stringify(data));
        localStorage.setItem('covest',result);
    },
    logout(state){
        localStorage.clear();
        state.user={};
        state.token=null;
    }
}
 