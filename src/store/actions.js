import axios from 'axios';

export default {  
    handleError(context,error){
        if(error.request.status == 422){
            var resp=JSON.parse(error.request.response);
            var err=resp.errors; 
            var msg='';
            for(var item in err){
                msg=err[item][0];
                break; // it picks the first error ; 
            }  
            context.commit('setNotification',{type:2,msg:msg});
            return msg;
        }
        else if(error.request.status == 404){
            resp=JSON.parse(error.request.response);
            // msg= "Route not found";
            // context.commit('setNotification',{type:2,msg:msg}); 
        }
        else if(error.request.status == 401){
            msg="Oops! Authentication error, Please login again";
            context.commit('setNotification',{type:2,msg:msg});
            // context.commit('logout');
            window.location.href="/login";
        }
        else {
            msg="Oops! server error, Please try again";
            context.commit('setNotification',{type:2,msg:msg});
        }
    },
    get(context, endpoint){ 
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+endpoint)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                console.log('error', error.request); 
                context.dispatch('handleError',error);
                reject(error)
            })
        });
    },
    getRaw(context, endpoint){ 
        return new Promise((resolve,reject)=>{
            axios.get(endpoint,{
                headers :{'Authorization':'Bearer '+process.env.VUE_APP_S}
            })
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                console.log('error', error.request); 
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },
    postRaw(context , data){ 
        return new Promise((resolve,reject)=>{
            axios.post(data.endpoint , data.details,
                {
                    headers :{'Authorization':'Bearer sk_test_7fac11a5fa1cd74abed6911b8ba3d7a9f74b7ac1'}
                })
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },
    post(context , data){ 
        return new Promise((resolve,reject)=>{
            axios.post(context.state.endpoint+data.endpoint , data.details,{
                // onUploadProgress: function( progressEvent ) {
                //     context.state.requestStatus.perc = parseInt( Math.round( ( progressEvent.loaded * 100 ) / progressEvent.total ) );
                //     // console.log(pro);
                //   }.bind(this)
            })
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                context.dispatch('handleError',error);
                reject(error)
            })
        });
    },
    progress(context , data){ 
        return new Promise((resolve,reject)=>{
            let id=Math.round(Math.random() * 999999);
            
            context.commit('createProgress',{id,title:"Photo uploading..."});
            axios.post(data.endpoint , data.details,{
                onUploadProgress: function( progressEvent ) {
                     context.commit('updateProgress',{id,value:parseInt( Math.round( ( progressEvent.loaded * 100 ) / progressEvent.total ) )});
                }.bind(this)
            })
            .then((data)=>{
                context.commit('deleteProgress',{id});
                resolve(data)
            })
            .catch((error)=>{
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    }, 
    put(context , data){
        return new Promise((resolve,reject)=>{
            axios.put(context.state.endpoint+data.endpoint , data.details)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    }, 
    delete(context , endpoint){
        return new Promise((resolve,reject)=>{
            axios.delete(context.state.endpoint+endpoint)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },
    checkIfSessionExpired(context , error){
        try{
            if(error.request.status == 401 || error.request.status == 400 || error.request.status == 403){
                  //token expired
                  context.commit('logout');
                //   alert("Your session has expired");
                    window.location='/';
                    // context.dispatch('relogin',context); 
              }
              else if(JSON.parse(error.request.response).message == "Token has expired"){
                  //token expired
                  context.commit('logout');
                //   alert("Your session has expired"); 
                    window.location='/';
                    // context.dispatch('relogin',context); 
              }
          }
          catch(e){
            //   
            context.dispatch('relogin',context); 
            context.commit('logout');
          }
    }, 
    relogin(context){ 
        context.commit('logout');
    //   alert("Your session has expired"); 
        window.location='/';
    },
    checkAuth(context){
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+'checkAuth?token='+context.state.token)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                // localStorage.removeItem('token');
                context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },
    getUser(context){
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+'get-user?token='+context.state.token)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },
    getDashboard(context){
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+'get-dashboard?token='+context.state.token)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },
}