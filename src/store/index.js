import Vue from 'vue';
import Vuex from 'vuex';

import mutations from './mutations';
import actions from './actions';
import getters from './getters';
 

Vue.use(Vuex);

const store=new Vuex.Store({
    state:{
        // endpoint:process.env.VUE_APP_ENDPOINT,  
        endpoint:"https://pre-server.covest.ng/api/v1/",  
        notification:{
            type:0,
            message:''
        },
        user:{fixed:{amount:0},stash:{amount:0}},
        token:'', 
        saving:{fixed:{},card:{},since:0},
        stash:{amount:0,hasActiveWithdrawal:false},
    },
    getters,
    mutations,
    actions
});

export default store;